// Patrick San Juan
// 301245902
// psanjuan@sfu.ca
// psanjuan
// Test Cases Performed: Cases 1 to 5 as specified by 
// https://courses.cs.sfu.ca/2016fa-cmpt-371-d2/pages/Prj1-testcases/view

import java.io.* ;
import java.net.* ;
import java.util.* ;

public final class WebServer {
    public static void main(String argv[]) throws Exception
    {
		//Listen for requests on port 6789
        int port = 6789;
        ServerSocket serverSocket;
        //Start serversocket
        try {
            serverSocket = new ServerSocket(port);
            System.out.print("Server is up and running \n");
        } catch (IOException exception){
            serverSocket = null;
            System.out.println(exception);
        }

        //Infinite loop to catch requests
        while (true){
            try {
                System.out.print("Waiting for request.\n");
                Socket socket = serverSocket.accept();      //accept requsts
                Thread newClientSocket = new Thread(new HttpRequest(socket));   //initialize new Thread
                newClientSocket.start();            //start new thread
            } catch (IOException exception){
                System.out.println(exception);
            }
        }
    }
}

//Thread class
final class HttpRequest implements Runnable
{
    final static String CRLF = "\r\n";
    private Socket socket;
    HttpRequest(Socket socket){
        System.out.print("I am a new thread\n");
        this.socket = socket;
    }

    @Override
    public void run() {
        try{
            processRequest();
        } catch (Exception exception){
            System.out.println(exception);
        }
    }

    private void processRequest() throws IOException {
        // Get a reference to the socket's input and output streams.
        InputStream inputStream = socket.getInputStream();
        DataOutputStream dataOutputStream = new DataOutputStream(socket.getOutputStream()) ;

        // Set up input stream filters.
        InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

        //Get Request Line
        String requestLine = bufferedReader.readLine();
        // Display the request line.
        System.out.println(requestLine);
        // Display the header lines
        String headerLine = null;
        while ((headerLine = bufferedReader.readLine()).length() != 0) {
            System.out.println(headerLine);
        }
        //Process Request in Request LIne
        // Extract the filename from the request line.
        StringTokenizer tokens = new StringTokenizer(requestLine);
        tokens.nextToken();  // skip over the method, which should be "GET"
        String fileName = tokens.nextToken();
        // Prepend a "." so that file request is within the current directory.
        fileName = "." + fileName;

        // Open the requested file.
        FileInputStream fis = null;
        boolean fileExists = true;
        try {
            fis = new FileInputStream(fileName);
        } catch (FileNotFoundException e) {
            fileExists = false;
        }

        //Construct response message
        String statusLine = null;
        String contentTypeLine = null;
        String entityBody = null;
        //If file exits, send appropriate response message, else, send the HTML content thats says ERROR
        if (fileExists) {
            statusLine = "Status: 200 OK" + CRLF;
            contentTypeLine = "Content-type:" + contentType(fileName) + CRLF;
        } else {
            statusLine = "Status: 400 Bad Request" + CRLF;
            contentTypeLine = "Content-type: text/html" + CRLF;
            entityBody = "<HTML>" + "<HEAD><TITLE>Error 404 - Not Found</TITLE></HEAD>" + "<BODY>Not Found</BODY></HTML>";
        }

        // Send the entity body.
        if (fileExists)	{
            try {
                sendBytes(fis, dataOutputStream);
            }catch (Exception e){
                //do nothing...
            }
            fis.close();
        } else {
            dataOutputStream.writeBytes(entityBody);
        }
        // Close streams and socket.
        dataOutputStream.close();
        bufferedReader.close();
        socket.close();
    }

    //Function to send content 
    private static void sendBytes(FileInputStream fis, OutputStream os) throws Exception
    {
        // Construct a 1K buffer to hold bytes on their way to the socket.
        byte[] buffer = new byte[1024];
        int bytes = 0;

        // Copy requested file into the socket's output stream.
        while((bytes = fis.read(buffer)) != -1 ) {
            os.write(buffer, 0, bytes);                 //send the bytes to the client
        }
    }

    private static String contentType(String fileName)
    {
        if(fileName.endsWith(".htm") || fileName.endsWith(".html")) {
            return "text/html";
        }
        if(fileName.endsWith(".jpg") || fileName.endsWith(".jpeg")) {
		    return "image/jpeg";
        }
        if(fileName.endsWith(".gif")) {
		    return "image/gif";
        }
        return "application/octet-stream";
    }

}